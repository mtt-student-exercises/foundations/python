def get_user_input(prompt):
    return input(prompt)

def validate_numeric_input(prompt):
    is_input_valid = False
    numeric_value = 0

    while not is_input_valid:
        newVal = get_user_input(prompt)

        try:
            numeric_value = int(newVal)

            is_input_valid = True

        except:
            print('Not Valid, Please Enter a Number (0-100)')

    return numeric_value

def get_numeric_input(prompt):
    return validate_numeric_input(prompt)