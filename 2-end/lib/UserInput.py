def get_user_input(prompt):
    return input(prompt)

def validate_numeric_input(prompt):
    is_input_valid = False
    numeric_value = 0

    while not is_input_valid:
        newVal = get_user_input(prompt)

        try:
            numeric_value = int(newVal)

            is_input_valid = True

        except:
            print('Not Valid, Please Enter a Number (0-100)')

    return numeric_value

def validate_range_input(prompt):
    while 1:
        newVal = get_user_input(prompt)
        if any(x in newVal.lower() for x in ['u','l','n','s','a']):
            return newVal
        else: print('Not Valid, Please Enter any combination of u,l,n,s,a')    


def get_numeric_input(prompt):
    return validate_numeric_input(prompt)

def get_range_input(prompt):
    return validate_range_input(prompt)