def convert_to_char(ascii_val):
    return chr(ascii_val)

def convert_to_ascii(char):
    return ord(char)